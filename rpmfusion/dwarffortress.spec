# Disable binary stripping.
# dfhack, dwarftherapist, and a number of other tools recognize
# the DF version by checksum. If we don't disable stripping, the
# checksums will differ and they won't work. Thus this is necessary.
%global __os_install_post %{nil}

# On CentOS, we want to strip the ".centos" bit of the dist tag.
# Credit to https://serverfault.com/questions/688485/rpm-dist-tag-not-behaving-as-documented
# This should go away when the package is imported to RPM Fusion
%if 0%{?rhel} == 7
	%define dist .el7
%endif

Name:           dwarffortress
Version:        0.43.05
Release:        4%{?dist}

Summary:        A single-player procedurally generated fantasy game

License:        Dwarf Fortress
URL:            http://www.bay12games.com/dwarves/
Source0:        http://www.bay12games.com/dwarves/df_43_05_linux.tar.bz2
Source1:        http://www.bay12games.com/dwarves/df_43_05_linux32.tar.bz2
Source2:        https://github.com/svenstaro/dwarf_fortress_unfuck/archive/0.43.05.zip
Source3:        https://www.acm.jhu.edu/~bjr/fedora/dwarffortress/rpmfusion/dwarffortress-launcher.tar.xz

# Only build for 32 and 64 bit x86 systems.
ExclusiveArch:  x86_64 %{ix86}

# BuildRequires from https://github.com/svenstaro/dwarf_fortress_unfuck/
BuildRequires:  cmake, libpng-devel, gcc, gcc-c++, automake
BuildRequires:  libXext-devel, atk-devel, cairo-devel, gdk-pixbuf2-devel,
BuildRequires:  fontconfig-devel, openal-soft-devel, SDL_image-devel, SDL_ttf-devel,
BuildRequires:  freetype-devel, libX11-devel, libICE-devel, libSM-devel, mesa-libGL,
BuildRequires:  mesa-libGL-devel, glib2-devel, mesa-libGLU-devel, pango-devel, ncurses-devel,
BuildRequires:  libsndfile-devel, gtk2-devel, glew-devel, SDL-devel, glibc-devel

# BuildRequires we need for the rest of the RPM.
BuildRequires:  desktop-file-utils, dos2unix, unzip

%description
Dwarf Fortress is a single-player fantasy game. You can control a dwarven
outpost or an adventurer in a randomly generated, persistent world.

Although Dwarf Fortress is still in a work in progress, many features
have already been implemented.

Dwarf Fortress is free to redistribute, but is not open source.

%prep
# Cleanup from previous build, if necessary.
#rm -rf df_linux/
#rm -rf %{buildroot}

# Extract the 'sources' into the build directory.
# This is architecture-dependent, because upstream distributes two tarballs.
%ifarch x86_64
tar xfj %SOURCE0
%else
tar xfj %SOURCE1
%endif
cd df_linux/
unzip -o %SOURCE2
tar xfJ %SOURCE3

# Fix some permissions.
find -type d -exec chmod 755 {} +
find -type f -exec chmod 644 {} +
dos2unix *.txt

# On EL6 fix things, don't use c++11.
# Hm, that does not work.
#%if 0%{?el6}
#sed 's/-Dlinux -std=c++11/-Dlinux -std=c++03/' -i dwarf_fortress_unfuck*/CMakeLists.txt
#%endif

%build
cd df_linux/
cd dwarf_fortress_unfuck*
mkdir -p build && cd build
cmake ..
make %{?_smp_mflags}

%install
cd df_linux/
mkdir -p %{buildroot}%{_datadir}/dwarffortress/
mkdir -p %{buildroot}%{_libexecdir}/dwarffortress/
cp -r data raw sdl %{buildroot}%{_datadir}/dwarffortress/

# Copy over the actual binary and compiled graphics library.
install -Dm755 libs/Dwarf_Fortress %{buildroot}%{_libexecdir}/dwarffortress/Dwarf_Fortress
install -Dm755 dwarf_fortress_unfuck*/build/libgraphics.so %{buildroot}%{_libexecdir}/dwarffortress/libgraphics.so

# Link libpng, because apparently we have to do this.
# Or... not.
# ln -s %{_libdir}/libpng.so %{buildroot}%{optdir}/dwarffortress/libs/libpng.so.3

# Install .desktop file and launcher script from Arch Linux package.
# Or, rather, the modified versions.
sed 's|prefix=/usr|prefix=%{_prefix}|' -i dwarffortress-launcher/dwarffortress
install -Dm755 dwarffortress-launcher/dwarffortress %{buildroot}%{_bindir}/dwarffortress
install -Dm644 dwarffortress-launcher/dwarffortress.desktop %{buildroot}%{_datadir}/applications/dwarffortress.desktop
install -Dm644 dwarffortress-launcher/dwarffortress.png %{buildroot}%{_datadir}/pixmaps/dwarffortress.png
desktop-file-validate %{buildroot}%{_datadir}/applications/dwarffortress.desktop

%files
%doc df_linux/*.txt df_linux/README.linux
%{_datadir}/dwarffortress
%{_libexecdir}/dwarffortress
%{_bindir}/dwarffortress
%{_datadir}/applications/dwarffortress.desktop
%{_datadir}/pixmaps/dwarffortress.png

%changelog
* Wed Feb 01 2017 Ben Rosser <rosser.bjr@gmail.com> 0.43.05-4
- Remove explicit dependency on alsa-plugins-pulseaudio, as it is no longer needed.

* Fri Jan 20 2017 Ben Rosser <rosser.bjr@gmail.com> 0.43.05-3
- Update launcher script to respect .stockpile file

* Thu Jul 7 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.05-2
- Minor spec fixes.

* Thu Jul 7 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.05-1
- Update to latest upstream release.
- 0.43.05 has 64-bit support (in a separate archive); add x86_64 to ExclusiveArch list
- Change "i686" to use the macro for 32-bit x86 architectures

* Tue Jun 21 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.04-1
- Update to latest upstream release.

* Fri Jun 17 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.03-2
- Disable stripping of binaries so dfhack, etc. can recognize DF version

* Fri May 27 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.03-1
- Update to latest upstream release.

* Thu May 12 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.02-1
- Update to latest upstream release.

* Tue May 10 2016 Ben Rosser <rosser.bjr@gmail.com> 0.43.01-1
- Update to latest upstream release.

* Wed Feb 10 2016 Ben Rosser <rosser.bjr@gmail.com> 0.42.06-1
- Update to latest upstream release.

* Sat Jan 23 2016 Ben Rosser <rosser.bjr@gmail.com> 0.42.05-3
- Split up dwarffortress rather than bundling it in /opt.
- Dwarf_Fortress binary is in libexecdir, along with compiled libgraphics.
- Data and raws have been moved into datadir.
- The dwarffortress script has been updated to refer to these locations.
- Added README.linux to documentation.

* Mon Jan 18 2016 Ben Rosser <rosser.bjr@gmail.com> 0.42.05-2
- Added explicit requires on alsa-plugins-pulseaudio that RPM failed to find.

* Mon Jan 18 2016 Ben Rosser <rosser.bjr@gmail.com> 0.42.05-1
- Upgraded to latest upstream release.

* Fri Jan  1 2016 Ben Rosser <rosser.bjr@gmail.com> 0.42.04-1
- Initial package, based heavily on the work done for Arch Linux.
